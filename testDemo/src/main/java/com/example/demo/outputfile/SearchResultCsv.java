package com.example.demo.outputfile;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;

@JsonPropertyOrder({"id","名前"})
@Data
public class SearchResultCsv {

	@JsonProperty
	private String id;

	@JsonProperty
	private String name;
	
	public SearchResultCsv() {
		super();
		// TODO Auto-generated constructor stub
	}
	public SearchResultCsv(String id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	
}
