package com.example.demo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.example.demo.entity.Users;

@Mapper
public interface UsersMapper {
	@Select("select id,username,sex,age,area,usertype,money,userdate from users where area = #{area};")
	List<Users> findByState(@Param("area") String area);
}
