package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import javax.swing.text.TableView.TableRow;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import com.example.common.UtilClass;
import com.example.demo.entity.Users;
import com.example.demo.outputfile.SearchResultCsv;
import com.example.form.SearchScreenForm;
import com.example.form.SearchScreenTableRow;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

@Service
public class SearchService {
	
	@Autowired
	private UsersDaoService usersDaoService;
	
	public SearchScreenForm init() {
		List<String> provinceList = UtilClass.getProvince();
		SearchScreenForm searchScreenForm = new SearchScreenForm();
		searchScreenForm.setProvinceList(provinceList.toArray(new String[provinceList.size()]));
		searchScreenForm.setInitFlag(true);
		return searchScreenForm;
	}
	
	public SearchScreenForm searchForHeader(SearchScreenForm searchScreenForm) {
		List<Users> users = usersDaoService.getUsersByArea(searchScreenForm.getArea());
		List<SearchScreenTableRow> tableRowsList = new ArrayList<SearchScreenTableRow>();
		//テーブルの行データをセットする
		for(Users user:users) {
			SearchScreenTableRow searchScreenTableRow = new SearchScreenTableRow();
			searchScreenTableRow.setId(user.getId());
			searchScreenTableRow.setUsername(user.getUsername());
			searchScreenTableRow.setArea(user.getArea());
			searchScreenTableRow.setSex(user.getSex());
			searchScreenTableRow.setAge(user.getAge());
			searchScreenTableRow.setUsertype(user.getUsertype());
			searchScreenTableRow.setMoney(user.getMoney());
			searchScreenTableRow.setUserdate(user.getUserdate().toString());
			
			tableRowsList.add(searchScreenTableRow);
		}
		//検索条件を詰めなおす
		searchScreenForm.setName(searchScreenForm.getName());
		searchScreenForm.setArea(searchScreenForm.getArea());
		List<String> provinceList = UtilClass.getProvince();
		searchScreenForm.setProvinceList(provinceList.toArray(new String[provinceList.size()]));
		
		//検索結果を詰め
		searchScreenForm.setSearchScreenTables(tableRowsList);
		return searchScreenForm;
	}
	
	public String getCsvContent(SearchScreenForm screenForm) throws JsonProcessingException {
		
		List<SearchResultCsv> list = new ArrayList<SearchResultCsv>();
		List<SearchScreenTableRow> tableList = screenForm.getSearchScreenTables();
		for(SearchScreenTableRow tableRow : tableList) {
			SearchResultCsv searchResultCsv = new SearchResultCsv();
			searchResultCsv.setId(tableRow.getId());
			searchResultCsv.setName(tableRow.getUsername());
			list.add(searchResultCsv);
		}
		
		CsvMapper mapper = new CsvMapper();
		CsvSchema schema = mapper.schemaFor(SearchResultCsv.class);

		return mapper.writer(schema).writeValueAsString(list);
	}
}
