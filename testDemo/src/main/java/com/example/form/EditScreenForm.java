package com.example.form;

public class EditScreenForm {

	private String editedIncoming;
	private String loan;
	private String mealFee;
	
	public String getIncoming() {
		return editedIncoming;
	}
	public void setIncoming(String incoming) {
		this.editedIncoming = incoming;
	}
	public String getLoan() {
		return loan;
	}
	public void setLoan(String loan) {
		this.loan = loan;
	}
	public String getMealFee() {
		return mealFee;
	}
	public void setMealFee(String mealFee) {
		this.mealFee = mealFee;
	}
}
